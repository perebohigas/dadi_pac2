//
//  AuthenticationViewController.swift
//  PR2
//
//  Copyright © 2018 UOC. All rights reserved.
//

import UIKit

class AuthenticationViewController: UIViewController, UITextFieldDelegate {
    @IBOutlet weak var firstField: UITextField!
    @IBOutlet weak var secondField: UITextField!
    @IBOutlet weak var thirdField: UITextField!
    @IBOutlet weak var fourthField: UITextField!
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        // We first check that the user is only entering numeric characters
        let numericSet = CharacterSet.decimalDigits
        let stringSet = CharacterSet(charactersIn: string)
        let onlyNumeric = numericSet.isSuperset(of: stringSet)
        
        if !onlyNumeric {
            return false
        }
        
        // We then check that the length of resulting text will be smaller or equal to 1
        let currentString = textField.text as NSString?
        let resultingString = currentString?.replacingCharacters(in: range, with: string)
        
        if let resultingLength = resultingString?.count, resultingLength <= 1 {
            return true
        } else {
            return false
        }
    }
    
    @IBAction func nextButtonTapped(_ sender: UIButton) {
        doAuthentication()
    }
    
    @IBAction func backButtonTapped(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    func doAuthentication() {
        var validCode: Bool
        if let firstCode = firstField.text, let secondCode = secondField.text, let thirdCode = thirdField.text, let fourthCode = fourthField.text {
            let fullCode = firstCode + secondCode + thirdCode + fourthCode
            validCode = Services.validate(code: fullCode)
        } else {
            validCode = false
        }
        
        if validCode {
            performSegue (withIdentifier: "SegueToMainNavigation", sender: self)
        } else {
            let errorMessage = "Sorry, the entered code is not valid"
            let errorTitle = "We could not autenticate you"
            Utils.show (Message: errorMessage, WithTitle: errorTitle, InViewController: self)
        }
    }
    
    // BEGIN-UOC-2

    @IBAction func firstFieldChange(_ sender: Any) {
        if !fieldIsEmpty(firstField) {
        secondField.becomeFirstResponder()
        }
    }
    
    @IBAction func secondFieldChange(_ sender: Any) {
        if !fieldIsEmpty(secondField) {
            thirdField.becomeFirstResponder()
        }
    }
    
    @IBAction func thirdFieldChange(_ sender: Any) {
        if !fieldIsEmpty(thirdField) {
            fourthField.becomeFirstResponder()
        }
    }
    
    @IBAction func fourthFieldChange(_ sender: Any) {
        if !fieldIsEmpty(fourthField) {
            doAuthentication()
        }
    }
    
    // Minor improvement: don't change focus when textView is empty
    func fieldIsEmpty(_ textField: UITextField) -> Bool {
        if (textField.text == "" || textField.text == nil) {
            return true
        } else {
            return false
        }
    }

    // Minor improvement: focus on first textView when this screen appears
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        firstField.becomeFirstResponder()
    }
    
    // END-UOC-2
    
}
