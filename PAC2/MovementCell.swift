//
//  MovementCellTableViewCell.swift
//  PR2
//
//  Copyright © 2018 UOC. All rights reserved.
//

import UIKit

class MovementCell: UITableViewCell {
    
    // BEGIN-UOC-4
    
    @IBOutlet weak var movementDescriptionLabel: UILabel!
    @IBOutlet weak var movementDateLabel: UILabel!
    @IBOutlet weak var movementAmountLabel: UILabel!
    
    // END-UOC-4
    
}
